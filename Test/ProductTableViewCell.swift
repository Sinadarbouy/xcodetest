//
//  ProductTableViewCell.swift
//  Test
//
//  Created by parsa on 8/27/18.
//  Copyright © 2018 parsa. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell{
    
   
    @IBOutlet weak var pr_image: UIImageView!
    
    @IBOutlet weak var pr_name: UILabel!
    @IBOutlet weak var pr_price: UILabel!
    
   
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        pr_image.clipsToBounds = true
        
        
    
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        pr_image.layer.cornerRadius = pr_image.frame.width / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
