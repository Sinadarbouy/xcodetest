//
//  extension.swift
//  Test
//
//  Created by Parsa Darbouy on 10/9/18.
//  Copyright © 2018 parsa. All rights reserved.
//

import UIKit

    extension UIView {

    func xibSetup(getView: UIView) {
        let view = loadViewFromNib(getView: getView)
//        view.frame = self.bounds
//        view.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.RawValue(UInt8(UIView.AutoresizingMask.flexibleWidth.rawValue) | UInt8(UIView.AutoresizingMask.flexibleHeight.rawValue)))
//
        self.addSubview(view)
    }
    
    
    fileprivate func loadViewFromNib(getView: UIView) -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: getView)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }

}
