//
//  productCollectionViewCell.swift
//  Test
//
//  Created by parsa on 8/28/18.
//  Copyright © 2018 parsa. All rights reserved.
//

import UIKit

class productCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var Collection_Image: UIImageView!
    @IBOutlet weak var Collection_Price: UILabel!
    @IBOutlet weak var collection_name: UILabel!
}
