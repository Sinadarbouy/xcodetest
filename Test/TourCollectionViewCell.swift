//
//  TourCollectionViewCell.swift
//  Test
//
//  Created by parsa on 9/5/18.
//  Copyright © 2018 parsa. All rights reserved.
//

import UIKit

class TourCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var Image_Tour: UIImageView!
    
    @IBOutlet weak var title_Tour: UILabel!
    @IBOutlet weak var Price_Tour: UILabel!
    @IBOutlet weak var name_Tour: UILabel!
}
