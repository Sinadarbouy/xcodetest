//
//  HomeViewController.swift
//  Test
//
//  Created by parsa on 9/25/18.
//  Copyright © 2018 parsa. All rights reserved.
//

import UIKit
import ImageSlideshow
import Alamofire
import SwiftyJSON

class HomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var tour_image_array : [UIImage] = [#imageLiteral(resourceName: "amazon-logo"),#imageLiteral(resourceName: "apple-logo"),#imageLiteral(resourceName: "Blocket-logo"),#imageLiteral(resourceName: "eBay-logo")]
    var tour_title_array : [String] = ["amazon","apple","blocket","ebay"]
    var tour_price_array : [String] = ["100","200","300","400"]
    var tour_date_array : [String] = ["100s","200s","300s","400s"]
    var AllTours : [TotalTour] = []
    
    
    //    var idArray = ["2" , "3" , "12" , "13"]
    
    //    var titleArray = ["ali" , "reza" , "gholi" , "goli" , "ahmad"]
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AllTours.count + 1
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.row == 0){
            let slidercell = tableView.dequeueReusableCell(withIdentifier: "SliderCell") as! Sliderimages
            
            
            return slidercell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TourCell") as! TourTableViewCell
            
            
            
            
            cell.LikeBtn.tag = indexPath.row - 1
            
            cell.LikeBtn.addTarget(self, action: #selector(likeAC(sender:)), for: .touchUpInside)
            
            
            
            let favobj = realm.objects(FavoritesDB.self)
            
            if favobj.filter("id == '\(AllTours[indexPath.row - 1].id)'").count == 0{
                cell.LikeBtn.setImage(#imageLiteral(resourceName: "like"), for: .normal)
            }else{
                cell.LikeBtn.setImage(#imageLiteral(resourceName: "like-2"), for: .normal)
            }
            
            cell.BtnSave.tag = indexPath.row - 1
            cell.BtnSave.addTarget(self, action: #selector(saveAC(sender:)), for: .touchUpInside)
            
            
            let saveObj = realm.objects(SavedDB.self)
            if saveObj.filter("id == '\(AllTours[indexPath.row - 1].id)'").count == 0{
                cell.BtnSave.setImage(#imageLiteral(resourceName: "unTag"), for: .normal)
            }else{
                cell.BtnSave.setImage(#imageLiteral(resourceName: "tag"), for: .normal)
            }
            
            cell.Tour_Title.text = AllTours[indexPath.row - 1].Title
            cell.Start_Date.text = AllTours[indexPath.row  - 1].start_date
            cell.Tour_Image.image = #imageLiteral(resourceName: "chitabe")
            
            
            
            //            cell.Tour_Image.image = tour_image_array[indexPath.row - 1]
            //
            //            cell.Tour_Title.text = tour_title_array[indexPath.row - 1]
            //
            //            cell.Start_Date.text = tour_date_array[indexPath.row - 1]
            return cell
            
        }
        
        
    }
    
    @objc func likeAC(sender: UIButton!) {
        print("show me tag \(sender.tag)")
        let ischeck = MyRealm.addOrDeleteFav(getID:String(AllTours[sender.tag].id), getTitle: AllTours[sender.tag].Title, getDate: "")
        if ischeck{
            sender.setImage(#imageLiteral(resourceName: "like-2"), for: .normal)
        }else{
            sender.setImage(#imageLiteral(resourceName: "like"), for: .normal)
        }
    }
    
    @objc func saveAC(sender: UIButton!) {
        let ischeck = MyRealm.addOrDeleteSave(getID: String(AllTours[sender.tag].id), getTitle: AllTours[sender.tag].Title, getDate: "")
        
        if ischeck{
            sender.setImage(#imageLiteral(resourceName: "tag"), for: .normal)
        }else{
            sender.setImage(#imageLiteral(resourceName: "unTag"), for: .normal)
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return self.view.frame.height * (0.3)
        }else{
            return self.view.frame.height * (0.5)
        }
    }
    
    
    @IBOutlet weak var tourTableView: UITableView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tourTableView.delegate = self
        tourTableView.dataSource = self
        
        ws_tours()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    private func ws_tours(){
        Alamofire.request("http://darbouy.ir/api/tours", method: .get).responseJSON { (response) in
            if response.result.value != nil {
                let GetJson = JSON(response.result.value!)
                let json = JSON(parseJSON: GetJson.stringValue)
                print("show me json \(json)")
                
                for item in json.arrayValue{
                    self.AllTours.append(TotalTour(Id:Int(item["Id"].stringValue)!,Title: item["Title"].stringValue, Tourtype: item["Type"].stringValue, Price: item["price"].stringValue, Start_date: item["Start_Date"].stringValue, image: item["url"].stringValue))
                }
                
                self.tourTableView.reloadData()
            }
        }
        
    }
    
    struct TotalTour {
        var id :Int
        var Title = ""
        var TourType = ""
        var Price = ""
        var start_date = ""
        var image = ""
        
        init(Id:Int,Title:String,Tourtype:String,Price:String,Start_date:String,image:String) {
            self.Title = Title
            self.TourType = Tourtype
            self.Price = Price
            self.start_date = Start_date
            self.image = image
            self.id = Id
        }
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

