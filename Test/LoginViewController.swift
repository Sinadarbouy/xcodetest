//
//  LoginViewController.swift
//  Test
//
//  Created by parsa on 8/27/18.
//  Copyright © 2018 parsa. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController , UIPickerViewDelegate, UIPickerViewDataSource {
    
    let phonenumber = "09174542538"
    let pass = "123456"
    @IBOutlet weak var phone_number: UITextField!
     @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var Pass_number: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
pickerView.delegate = self
        pickerView.dataSource = self
        // Do any additional setup after loading the view.
    }

    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 100
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(row)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(row)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func Alert(title:String,Message:String){
        
        var alert = UIAlertController(title: title, message: Message, preferredStyle: UIAlertControllerStyle.alert)
        
        var action = UIAlertAction(title: "بستن", style: UIAlertActionStyle.default) { (_) in
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(action)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    @IBAction func LoginBtn(_ sender: UIButton) {
        if phone_number.text! == phonenumber && Pass_number.text == pass{
            let VC = storyboard?.instantiateViewController(withIdentifier: "Main3VC")
            UserDefaults.standard.set(Pass_number.text, forKey: "passnumber")
            UserDefaults.standard.set(phone_number.text, forKey: "phonenumber")
            self.present(VC!, animated: true, completion: nil)
        }else{
            Alert(title: "توجه", Message:"ارور")
        }
        
    }
    
}
