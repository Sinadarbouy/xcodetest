//
//  text.swift
//  Test
//
//  Created by Parsa Darbouy on 10/9/18.
//  Copyright © 2018 parsa. All rights reserved.
//

import UIKit
@IBDesignable
class text: UIView {
    
    @IBOutlet weak var ToastLable: UILabel!
    
    
    @IBInspectable
    var textcolor : UIColor =  UIColor.red{
        didSet{
            
            ToastLable.textColor = textcolor
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup(getView: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup(getView: self)
    }
    
    
    
    
    
    
}
