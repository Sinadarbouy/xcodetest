//
//  TourTableViewCell.swift
//  Test
//
//  Created by parsa on 9/25/18.
//  Copyright © 2018 parsa. All rights reserved.
//

import UIKit

class TourTableViewCell: UITableViewCell {

    @IBOutlet weak var Tour_Title: UILabel!
    
    @IBOutlet weak var Tour_Image: UIImageView!
    
    @IBOutlet weak var Start_Date: UILabel!
    
    
    
    
    
    
    @IBOutlet weak var BtnSave: UIButton!
   
    @IBOutlet weak var LikeBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }

}
