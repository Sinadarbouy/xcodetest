//
//  MyRealm.swift
//  MarketTestProject
//
//  Created by SDD on 10/2/18.
//  Copyright © 2018 SDD. All rights reserved.
//

import Foundation
import RealmSwift

var realm = try! Realm()

public class FavoritesDB : Object {
    @objc dynamic var id = ""
    @objc dynamic var tourTitle = ""
    @objc dynamic var date = ""
}


public class SavedDB : Object {
    @objc dynamic var id = ""
    @objc dynamic var title = ""
    @objc dynamic var date = ""
}



class MyRealm {
    
    //    static func addToFav(getID: String , getTitle: String , getDate: String) {
    //
    //        let favObj = FavoritesDB()
    //        favObj.id = getID
    //        favObj.tourTitle = getTitle
    //        favObj.date = getDate
    //
    //
    //        try! realm.write {
    //            realm.add(favObj)
    //        }
    //    }
    //
    //    static func addToSave(getID: String , getTitle: String , getDate: String) {
    //
    //        let saveObj = SavedDB()
    //        saveObj.id = getID
    //        saveObj.title = getTitle
    //        saveObj.date = getDate
    //
    //
    //        try! realm.write {
    //            realm.add(saveObj)
    //        }
    //
    //
    //    }
    
    static func addOrDeleteSave(getID: String , getTitle: String , getDate: String) -> Bool {
        
        var saveObj = realm.objects(SavedDB.self)
        
        if saveObj.filter("id == '\(getID)'").count == 0 {
            
            let saveObj = SavedDB()
            saveObj.id = getID
            saveObj.title = getTitle
            saveObj.date = getDate
            
            
            try! realm.write {
                realm.add(saveObj)
            }
            return true
            
        } else {
            saveObj = realm.objects(SavedDB.self).filter("id == '\(getID)'")
            
            try! realm.write {
                realm.delete(saveObj)
            }
            return false
        }
        
    }
    
    
    
    static func addOrDeleteFav(getID: String , getTitle: String , getDate: String) -> Bool {
        
        var favObj = realm.objects(FavoritesDB.self)
        
        if favObj.filter("id == '\(getID)'").count == 0 {
            
            let favsObj = FavoritesDB()
            favsObj.id = getID
            favsObj.tourTitle = getTitle
            favsObj.date = getDate
            
            
            try! realm.write {
                realm.add(favsObj)
            }
            return true
            
            
            
        } else {
            favObj = realm.objects(FavoritesDB.self).filter("id == '\(getID)'")
            
            try! realm.write {
                realm.delete(favObj)
            }
            return false
        }
        
        
    }
    
    
    //    static func deleteFavDB(deleteBy id: String) {
    //
    //        let favObj = realm.objects(FavoritesDB.self).filter("id == '\(id)'")
    //
    //        try! realm.write {
    //            realm.delete(favObj)
    //        }
    //
    //
    //    }
    //
    //
    //    static func deleteSaveDB(deleteBy id: String? = nil) {
    //
    //        var saveObj = realm.objects(SavedDB.self)
    //
    //        if id == nil {
    //            saveObj = realm.objects(SavedDB.self)
    //        } else {
    //            saveObj = realm.objects(SavedDB.self).filter("id == '\(id!)'")
    //        }
    //
    //
    //        try! realm.write {
    //            realm.delete(saveObj)
    //        }
    //
    //
    //    }
    
    
    //func getFromDB() {
    //
    //    var getIDArray = [String]()
    //
    //    let favObj = realm.objects(FavoritesDB.self)
    //
    //    for item in favObj {
    //        //        item.id
    //        //        item.date
    //        //        item.tourTitle
    //
    //        getIDArray.append(item.id)
    //    }
    //
    //    print(getIDArray)
    //}
    
    
    
}

