//
//  Main2ViewController.swift
//  Test
//
//  Created by parsa on 8/28/18.
//  Copyright © 2018 parsa. All rights reserved.
//

import UIKit

class Main2ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    @IBOutlet weak var collection_view: UICollectionView!
    var pr_image_array : [UIImage] = [#imageLiteral(resourceName: "amazon-logo"),#imageLiteral(resourceName: "apple-logo"),#imageLiteral(resourceName: "Blocket-logo"),#imageLiteral(resourceName: "eBay-logo")]
    var pr_name_array : [String] = ["amazon","apple","blocket","ebay"]
    var pr_price_array : [String] = ["100","200","300","400"]
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pr_name_array.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "mainVC")
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "product", for: indexPath) as! productCollectionViewCell
        
        cell.Collection_Image.image = pr_image_array[indexPath.row]
        cell.collection_name.text = pr_name_array[indexPath.row]
        cell.Collection_Price.text = pr_price_array[indexPath.row]
        return cell
    }
    

    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collection_view.delegate = self
        collection_view.dataSource = self
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
