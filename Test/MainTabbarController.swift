//
//  MainTabbarController.swift
//  Test
//
//  Created by parsa on 9/25/18.
//  Copyright © 2018 parsa. All rights reserved.
//

import UIKit

class MainTabbarController: UITabBarController {
    
    
    
    private func SetupTab(){
        let firstVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC")
        let firstNvc = UINavigationController()
        firstNvc.viewControllers = [firstVC!]
        
        let secondVC = self.storyboard?.instantiateViewController(withIdentifier: "main2VC") as! Main2ViewController
        let secondNVC = UINavigationController()
        secondNVC.viewControllers=[secondVC]
        let thirdVC = self.storyboard?.instantiateViewController(withIdentifier: "Main3VC") as! Main3ViewController
        let thirdNVC = UINavigationController()
        thirdNVC.viewControllers=[thirdVC]
        let lastVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC")
        let lastNVC = UINavigationController()
        lastNVC.viewControllers=[lastVC!]
        let tabbars = [firstNvc,secondNVC,thirdNVC,lastNVC]
        
        self.viewControllers = tabbars as [UIViewController]

        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        SetupTab()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
