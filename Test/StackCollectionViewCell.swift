//
//  StackCollectionViewCell.swift
//  Test
//
//  Created by parsa on 9/3/18.
//  Copyright © 2018 parsa. All rights reserved.
//

import UIKit

class StackCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var image_stack: UIImageView!
    
    @IBOutlet weak var name_stack: UILabel!
    
    @IBOutlet weak var price_stack: UILabel!
}
