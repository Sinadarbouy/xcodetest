//
//  Sliderimages.swift
//  Test
//
//  Created by parsa on 10/2/18.
//  Copyright © 2018 parsa. All rights reserved.
//

import UIKit
import ImageSlideshow

class Sliderimages: UITableViewCell {

    
    @IBOutlet weak var slider: ImageSlideshow!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        slider.setImageInputs([
            ImageSource(image:#imageLiteral(resourceName: "Blocket-logo")),
            ImageSource(image:#imageLiteral(resourceName: "chitabe")),
            ImageSource(image:#imageLiteral(resourceName: "apple-logo")),
            ImageSource(image:#imageLiteral(resourceName: "amazon-logo"))
            ])
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
