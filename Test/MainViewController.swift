//
//  MainViewController.swift
//  Test
//
//  Created by parsa on 8/27/18.
//  Copyright © 2018 parsa. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var ProductTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        ProductTableView.delegate = self 
        ProductTableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    var pr_image_array : [UIImage] = [#imageLiteral(resourceName: "amazon-logo"),#imageLiteral(resourceName: "apple-logo"),#imageLiteral(resourceName: "Blocket-logo"),#imageLiteral(resourceName: "eBay-logo")]
    var pr_name_array : [String] = ["amazon","apple","blocket","ebay"]
    var pr_price_array : [String] = ["100","200","300","400"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pr_name_array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell") as! ProductTableViewCell
        cell.pr_image.image = pr_image_array[indexPath.row]
        cell.pr_name.text = pr_name_array[indexPath.row]
        cell.pr_price.text = pr_price_array[indexPath.row]
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
