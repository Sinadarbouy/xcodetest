//
//  Main3ViewController.swift
//  Test
//
//  Created by parsa on 9/3/18.
//  Copyright © 2018 parsa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
struct product {
    var name = ""
    var price = ""
    
    init(name : String ,price :String) {
        self.name = name
        self.price = price
    }
}

class Main3ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    @IBOutlet weak var firstCollectionView: UICollectionView!
    @IBOutlet weak var secondCollectionView: UICollectionView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
   
    
    
    var pr_image_array : [UIImage] = [#imageLiteral(resourceName: "amazon-logo"),#imageLiteral(resourceName: "apple-logo"),#imageLiteral(resourceName: "Blocket-logo"),#imageLiteral(resourceName: "eBay-logo")]
    var pr_name_array : [String] = []
    var pr_price_array : [String] = []
    
    var productobj = [product]()
    
    var pr_image_array2 : [UIImage] = [#imageLiteral(resourceName: "amazon-logo"),#imageLiteral(resourceName: "apple-logo"),#imageLiteral(resourceName: "Blocket-logo"),#imageLiteral(resourceName: "eBay-logo")]
    var pr_name_array2 : [String] = ["amazon2","apple2","blocket2","ebay2"]
    var pr_price_array2 : [String] = ["1000","2000","3000","4000"]
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == secondCollectionView {
            return pr_name_array2.count
            
        }else{
            return productobj.count
        }
        
        
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "main2VC")
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == secondCollectionView {
            
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "TourCVC", for: indexPath) as! TourCollectionViewCell
            cell2.Image_Tour.image = pr_image_array2[indexPath.row]
            cell2.name_Tour.text = pr_name_array2[indexPath.row]
            cell2.Price_Tour.text = pr_price_array2[indexPath.row]
            cell2.title_Tour.text = "title"+String(indexPath.row)
            return cell2
        }else{
            
            
            
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StackCV", for: indexPath) as! StackCollectionViewCell
            
            cell.image_stack.image = pr_image_array[indexPath.row]
            
            cell.name_stack.text = productobj[indexPath.row].name
            
            cell.price_stack.text = productobj[indexPath.row].price
            return cell
        }
        
        
        
        
    }
    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
   
    var refresher =  UIRefreshControl()


    
    
    @objc func doitem(){
        print("Click")
        //takeScreenshot()
        
    }
    
    
    func setnavitem(){
        let customright = UIBarButtonItem(title: "Do", style: .done, target: self, action: #selector(doitem))
        self.navigationItem.setRightBarButton(customright, animated: true)
    }
    
    
    @IBOutlet weak var ScrollViewHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = ""
        refresher.addTarget(self, action: #selector(ws_tours), for: .valueChanged)
        self.ws_tours()
        setnavitem()
        
        secondCollectionView.addSubview(refresher)
        //firstCollectionView.addSubview(refresher)
        self.loader.startAnimating()

        // Do any additional setup after loading the view.
    }
    
    @objc func ws_tours(){
        refresher.beginRefreshing()
         print("enter loop")
        let baseURl = "http://kargarin.netrou.ir/webservice/"
        let mainindex = "main/index"
        let param = [
            "mobile":"09177066896","password":"5954"
        ]
        
        Alamofire.request(baseURl + mainindex, method: .post, parameters: param).responseJSON { (response) in
            self.refresher.endRefreshing()
            if response.result.value != nil{
                
                self.loader.stopAnimating()
                print(response)
                if let data = response.data {
                    let json = try! JSON(data: data)
                    print("show me tours json \(json)")
                    
                    for item in json["provinces"].arrayValue.filter({ (jsonitem) -> Bool in
                        if jsonitem["id"].intValue < 5{
                            return true
                        }else{
                            return false
                        }
                    }){
                        self.productobj.append(product(name: item["province"].stringValue, price: item["id"].stringValue))
//                        self.pr_name_array.append(item["province"].stringValue)
//                        self.pr_price_array.append(item["id"].stringValue)
                    }
                    self.firstCollectionView.delegate = self
                    self.firstCollectionView.dataSource = self

                    self.secondCollectionView.delegate = self
                    self.secondCollectionView.dataSource = self
                    self.firstCollectionView.reloadData()
                    self.secondCollectionView.reloadData()
                }
                
            }else{
                print("nil")
            }
        }
    }
    
    
    
    
    

    @IBOutlet weak var StackView: UIStackView!
    
    
    
    

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        ScrollViewHeight.constant = StackView.frame.height + 20
        
    }
    
}
